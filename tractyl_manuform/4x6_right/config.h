/*
Copyright 2012 Jun Wako <wakojun@gmail.com>
Copyright 2015 Jack Humbert

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#define USB_POLLING_INTERVAL_MS 5
#define USB_VBUS_PIN        A9

#define ROTATIONAL_TRANSFORM_ANGLE  -100
#define POINTING_DEVICE_INVERT_X

#define DYNAMIC_KEYMAP_LAYER_COUNT  16
#define LAYER_STATE_16BIT

/* disable action features */
#define POINTING_DEVICE_RIGHT

#define SPLIT_HAND_PIN A10
#define SPLIT_TRANSPORT_MIRROR

#define TAPPING_TERM 200
#define PERMISSIVE_HOLD

/* serial.c configuration for split keyboard */
#define SERIAL_USART_FULL_DUPLEX  // Enable full duplex operation mode.
#define SERIAL_USART_TX_PIN      A2
#define SERIAL_USART_RX_PIN      A3
#define SERIAL_USART_DRIVER      SD2
#define SERIAL_USART_TX_PAL_MODE 7    // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7
#define SERIAL_USART_RX_PAL_MODE 7    // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7

#define CRC8_USE_TABLE
#define CRC8_OPTIMIZE_SPEED


/* spi config for flash, eeprom, lcd and pmw3389 sensor */
#define SPI_DRIVER                           SPID1
#define SPI_SCK_PIN                          A5
#define SPI_SCK_PAL_MODE                     5
#define SPI_MOSI_PIN                         A7
#define SPI_MOSI_PAL_MODE                    5
#define SPI_MISO_PIN                         A6
#define SPI_MISO_PAL_MODE                    5

/* pmw3389 config  */
#define PMW33XX_CS_PIN                       C10

/* f411 config (8MHz, no blackpill)  */
#define STM32_HSECLK 8000000

// SPI LCD Configuration
#define ST7735_NUM_DEVICES 1
#define LCD_RST_PIN B6
#define LCD_CS_PIN B10
#define LCD_DC_PIN B0

#ifndef LCD_ACTIVITY_TIMEOUT
    #define LCD_ACTIVITY_TIMEOUT 60000
#endif
#ifdef LCD_ACTIVITY_TIMEOUT
	#undef LCD_ACTIVITY_TIMEOUT
    #define LCD_ACTIVITY_TIMEOUT 60000
#endif

// Backlight driver (to control LCD backlight)
#define BACKLIGHT_PWM_DRIVER PWMD2
#define BACKLIGHT_PWM_CHANNEL 2
#define BACKLIGHT_PAL_MODE 1

//#define QUANTUM_PAINTER_SUPPORTS_256_PALETTE TRUE
//#define QUANTUM_PAINTER_SUPPORTS_NATIVE_COLORS TRUE
